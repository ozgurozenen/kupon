﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kupon.Models
{
    public class Kategori
    {
        public int Id { get; set; }
        public string Baslik { get; set; }
        public string Aciklama { get; set; }
    }
}